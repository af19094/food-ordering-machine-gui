import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

public class FoodOrderingMachineGUI {
    private JTextPane receivedinfo;
    private JButton checkOutButton;
    private JButton ramenButton;
    private JButton gyozaButton;
    private JButton karaageButton;
    private JButton tempuraButton;
    private JButton yakisobaButton;
    private JButton udonButton;
    private JPanel root;
    private JLabel price;
    private int total=0;

    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodOrderingMachineGUI");
        frame.setContentPane(new FoodOrderingMachineGUI().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    void order(String food){
        String orderedlist=receivedinfo.getText();
        int confirmation = JOptionPane.showConfirmDialog(null,"Would you like to order "+food+"?","Order Confirmation",
                JOptionPane.YES_NO_OPTION);
        if(confirmation==0) {
            JOptionPane.showMessageDialog(null, "Thank you for ordering " +food+ "! It will be served as soon as possible.");
            receivedinfo.setText(orderedlist+ "\n" +food);
            total+=100;
        }
        price.setText(total + " yen");
    }

    public FoodOrderingMachineGUI() {
        tempuraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tempura");
            }
        });
        karaageButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Karaage");
            }
        });
        gyozaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Gyoza");
            }
        });
        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Udon");
            }
        });
        yakisobaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Yakisoba");
            }
        });
        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Ramen");
            }
        });
        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,"Would you like to checkout?","Checkout Confirmation",
                        JOptionPane.YES_NO_OPTION);
                if(confirmation==0) {
                    JOptionPane.showMessageDialog(null, "Thank you. The total price is " +total+ " yen.");
                    receivedinfo.setText(null);
                    total=0;
                }
                price.setText(total + " yen");
            }
        });
        root.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                super.componentResized(e);
            }
        });
    }
}
